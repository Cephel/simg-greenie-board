import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PassesComponent } from './passes/passes.component';
import { AddPilotComponent } from './add-pilot/add-pilot.component';

const routes: Routes = [
	{ path: '', component: PassesComponent },
	{ path: 'login', component: LoginComponent },
	{ path: 'add', component: AddPilotComponent },

	{ path: '**', redirectTo: '' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
