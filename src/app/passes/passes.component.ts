import { Component, OnInit } from '@angular/core';
import { CrudService } from '../services/crud.service';
import { Observable } from 'rxjs';
import { Pilot } from '../services/pilot';
import { Trap } from '../services/trap';


@Component({
	selector: 'simg-passes',
	templateUrl: './passes.component.html'
})

export class PassesComponent implements OnInit {

	pilotList: Pilot[];

	constructor(public crud: CrudService) { }

	ngOnInit() {
		this.crud.GetPilotList().subscribe(list => {
			this.pilotList = list;
			this.getTraps();
		});
	}

	getTraps() {
		this.pilotList.forEach(
			pilot => this.crud.GetPassesForPilot(pilot.$id).subscribe(
				passesList => pilot.traps = passesList
			)
		);
	}
}
