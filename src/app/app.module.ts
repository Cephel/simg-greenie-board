import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PassComponent } from '../modules/pass/pass.component';
import { PassesComponent } from './passes/passes.component';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { CommonModule, AsyncPipe } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { AddPilotComponent } from './add-pilot/add-pilot.component';

@NgModule({
	declarations: [
		AppComponent,
		PassComponent,
		PassesComponent,
		LoginComponent,
		HeaderComponent,
		AddPilotComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		BrowserModule,
		AppRoutingModule,
		AngularFireModule.initializeApp(environment.firebaseConfig, 'simg-greenie-board'),
		AngularFirestoreModule,
		AngularFireAuthModule,
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
