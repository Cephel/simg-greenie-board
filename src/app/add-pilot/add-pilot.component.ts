import { Component, OnInit } from '@angular/core';
import { CrudService } from '../services/crud.service';

@Component({
	selector: 'simg-add-pilot',
	templateUrl: './add-pilot.component.html',
	styleUrls: ['./add-pilot.component.scss']
})
export class AddPilotComponent implements OnInit {

	constructor(public crud: CrudService) { }

	ngOnInit() {
	}

}
