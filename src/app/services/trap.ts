export class Trap {
	constructor(
		public $id: string,
		public $pilotID: string,
		public score: number,
		public nightPass: boolean = false
	) { }
}
