import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { Router } from '@angular/router';


@Injectable({
	providedIn: 'root'
})
export class AuthService {
	user: User;

	constructor(private auth: AngularFireAuth, private router: Router) {
		this.auth.authState.subscribe(user => {
			if (user) {
				this.user = user;
				localStorage.setItem('user', JSON.stringify(this.user));
			} else {
				localStorage.setItem('user', null);
			}
		});
	}

	async login(email: string, password: string) {
		const result = await this.auth.auth.signInWithEmailAndPassword(email, password);
		this.router.navigate(['']);
	}

	async logout() {
		await this.auth.auth.signOut();
		localStorage.removeItem('user');
		this.router.navigate(['']);
	}
}
