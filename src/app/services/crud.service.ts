import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Pilot } from './pilot';
import { Trap } from './trap';


@Injectable({
	providedIn: 'root'
})
export class CrudService {

	constructor(private db: AngularFirestore) { }

	GetPilotList(): Observable<Pilot[]> {
		return this.db.collection<Pilot>('pilots').valueChanges();
	}

	GetPassesForPilot($pilotID: string): Observable<Trap[]> {
		return this.db.collection('traps').doc($pilotID).collection<Trap>('traps').valueChanges();
	}

	AddPilot(name: string, avatarUrl: string) {
		const $id = this.db.createId();
		this.db.collection<Pilot>('pilots').doc($id).set({
			$id,
			name,
			avatarUrl
		});
	}

	RemovePilot($pilotID: string) {
		this.db.collection<Pilot>('pilots').doc($pilotID).delete();
	}

	AddPass($pilotID: string, score: number, night: boolean = false) {
		const $id = this.db.createId();
		this.db.collection('traps').doc($pilotID).collection('traps').doc<Trap>($id).set({
			$id,
			$pilotID,
			score,
			nightPass: night
		});
	}
}
