import { Trap } from './trap';


export class Pilot {
	public traps: Trap[];

	constructor(
		public $id: string,
		public name: string,
		public avatarUrl: string
	) {
		this.traps = new Array<Trap>();
	}
}
