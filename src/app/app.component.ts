import { Component } from '@angular/core';

@Component({
	selector: 'simg-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
	title = 'simg-greenie-board';
}
