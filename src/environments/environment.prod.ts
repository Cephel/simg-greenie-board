export const environment = {
	production: true,

	// Initialize Firebase
	firebaseConfig: {
		apiKey: 'AIzaSyBBYjBEfMO_8lftlnltmOOcfh0qnvwkbqY',
		authDomain: 'simg-greenie-board.firebaseapp.com',
		databaseURL: 'https://simg-greenie-board.firebaseio.com',
		projectId: 'simg-greenie-board',
		storageBucket: 'simg-greenie-board.appspot.com',
		messagingSenderId: '473453491649'
	}
};
