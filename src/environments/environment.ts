// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,

	// Initialize Firebase
	firebaseConfig: {
		apiKey: 'AIzaSyBBYjBEfMO_8lftlnltmOOcfh0qnvwkbqY',
		authDomain: 'simg-greenie-board.firebaseapp.com',
		databaseURL: 'https://simg-greenie-board.firebaseio.com',
		projectId: 'simg-greenie-board',
		storageBucket: 'simg-greenie-board.appspot.com',
		messagingSenderId: '473453491649'
	}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
